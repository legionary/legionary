package game

import (
	"time"

	"gitlab.com/legionary/legionary/core/graph"
	"gitlab.com/legionary/legionary/core/math/vector"
	"gitlab.com/legionary/legionary/game/object"
	"gitlab.com/legionary/legionary/message"
)

var nodes map[int64]*Node

func init() {
	nodes = make(map[int64]*Node)
}

type Node struct {
	id               int64
	world            *World
	position         vector.Vector
	subscribeUserIds map[int64]int64
	events           chan func(node *Node)
	entitys          map[int64]*object.Entity
}

func NewNode(id int64, world *World, position vector.Vector) *Node {
	node := &Node{
		id,
		world,
		position,
		make(map[int64]int64),
		make(chan func(node *Node)),
		make(map[int64]*object.Entity)}

	nodes[node.ID()] = node
	return node
}

func GetNode(id int64) *Node {
	return nodes[id]
}

func (node Node) ID() int64 {
	return node.id
}

func (node Node) Position() vector.Vector {
	return node.position
}

func (node Node) Len(o graph.Node) float64 {
	return o.Position().Sub(node.Position()).Size()
}

func (node *Node) Update(now time.Time) {
	for _, entity := range node.entitys {
		entity.Update(now)
	}

	for {
		select {
		case event := <-node.events:
			event(node)
		default:
			return
		}
	}
}

func (node *Node) Subscribe(userID int64) {
	node.addEvent(
		func(node *Node) {
			node.subscribeUserIds[userID] = userID
		})
}

func (node *Node) CreateEntity(userID int64) {
	node.addEvent(
		func(node *Node) {
			entity := object.NewEntity(node, userID)
			node.entitys[entity.ID()] = entity

			entityCreate := &message.EntityCreatePush{
				[]message.EntityCreate{{
					node.id,
					message.Entity{
						entity.ID(),
						entity.UserID(),
						"Entity"}}}}
			node.send(entityCreate)
		})
}

func (node *Node) AddEntity(fromNodeId int64, entity *object.Entity) {
	node.addEvent(
		func(node *Node) {
			node.entitys[entity.ID()] = entity

			entityMovePush := &message.EntityMovePush{
				entity.ID(),
				fromNodeId,
				node.id}
			node.send(entityMovePush)
		})
}

func (node Node) Entitys() map[int64]*object.Entity {
	return node.entitys
}

func (node Node) FindPath() graph.Path {
	return node.world.FindPath(node.id, 8)
}

func (node *Node) RequestMove(entity *object.Entity, nodeID int64) {
	node.addEvent(
		func(node *Node) {
			delete(node.entitys, entity.ID())

			targetNode := GetNode(nodeID)
			if targetNode == nil {
				return
			}

			targetNode.AddEntity(node.id, entity)
			entity.SetNode(targetNode)
		})
}

func (node *Node) addEvent(event func(node *Node)) {
	go func() {
		node.events <- event
	}()
}

func (node *Node) send(message Message) {
	for id := range node.subscribeUserIds {
		user := GetUser(id)
		if user == nil {
			delete(node.subscribeUserIds, id)
			continue
		}

		user.Send(message)
	}
}
