package game

import (
	"encoding/binary"
	"fmt"
	"log"

	"github.com/gorilla/websocket"
	"gitlab.com/legionary/legionary/core/id_generator"
	"gitlab.com/legionary/legionary/message"
)

var users map[int64]*User

func init() {
	users = make(map[int64]*User)
}

type User struct {
	id    int64
	conn  *websocket.Conn
	world *World
}

func NewUser(conn *websocket.Conn, world *World) *User {
	user := &User{
		id_generator.Generate(),
		conn,
		world,
	}

	return user
}

func GetUser(id int64) *User {
	return users[id]
}

func (user User) ID() int64 {
	return user.id
}

func (user User) Send(message Message) {
	var head [4]byte
	binary.LittleEndian.PutUint32(head[0:], message.MessageID())

	body, err := message.MarshalMsg(nil)
	if err != nil {
		log.Println(err)
		return
	}

	user.conn.WriteMessage(websocket.BinaryMessage, append(head[:], body...))
}

func (user *User) OnCreated() {
	users[user.ID()] = user
}

func (user User) OnMessage(bytes []byte) {
	idBytes := bytes[:4]
	messageBytes := bytes[4:]

	messageID := binary.LittleEndian.Uint32(idBytes)
	switch messageID {
	case message.MessageEntityCreateReq:
		var entityCreateReq message.EntityCreateReq
		entityCreateReq.UnmarshalMsg(messageBytes)

		node := GetNode(entityCreateReq.NodeID)
		if node == nil {
			return
		}

		node.CreateEntity(user.id)
	case message.MessageWorldReq:
		worldRes := &message.WorldRes{}
		worldRes.Graph = user.world.MessageGraph()
		for _, node := range worldRes.Graph.Nodes {
			node := GetNode(node.ID)
			if node == nil {
				continue
			}

			node.Subscribe(user.id)
		}
		user.Send(worldRes)

		entityCreate := &message.EntityCreatePush{user.world.EntityCreates()}
		user.Send(entityCreate)
	default:
		fmt.Println(messageID)
	}
}

func (user *User) OnClosed() {
	delete(users, user.ID())
}
