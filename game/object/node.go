package object

import "gitlab.com/legionary/legionary/core/graph"

type Node interface {
	FindPath() graph.Path
	RequestMove(entity *Entity, nodeID int64)
}
