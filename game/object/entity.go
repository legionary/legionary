package object

import (
	"time"

	"fmt"

	"gitlab.com/legionary/legionary/core/graph"
	"gitlab.com/legionary/legionary/core/id_generator"
)

// Entity is basic object for game.
type Entity struct {
	id            int64
	node          Node
	userID        int64
	nextMoveTime  time.Time
	path          graph.Path
	onRequestMove bool
}

func NewEntity(node Node, userID int64) *Entity {
	entity := &Entity{
		id_generator.Generate(),
		node,
		userID,
		time.Now(),
		graph.Path{},
		false}

	return entity
}

func (entity Entity) ID() int64 {
	return entity.id
}

func (entity Entity) UserID() int64 {
	return entity.userID
}

func (entity *Entity) Update(now time.Time) {
	if entity.path.Empty() {
		if entity.onRequestMove {
			return
		}

		entity.path = entity.node.FindPath()
	} else {
		now := time.Now()
		if now.After(entity.nextMoveTime) {
			fmt.Println(entity.path)
			entity.nextMoveTime = now.Add(time.Second)

			id := entity.path.Pop()
			if id == nil {
				return
			}

			entity.requestMove(*id)
		}
	}
}

func (entity *Entity) SetNode(node Node) {
	entity.onRequestMove = false
	entity.node = node
}

func (entity *Entity) requestMove(nodeID int64) {
	entity.onRequestMove = true
	entity.node.RequestMove(entity, nodeID)
}
