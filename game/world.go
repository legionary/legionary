package game

import (
	"time"

	"gitlab.com/legionary/legionary/core/graph"
	"gitlab.com/legionary/legionary/core/math/vector"
	"gitlab.com/legionary/legionary/message"
)

// World represents game.
type World struct {
	graph   *graph.Graph
	stopper chan bool
}

func NewWorld() *World {
	world := &World{}
	world.createGraph()

	return world
}

func (world *World) Update(now time.Time) {
	for nodeID := range world.graph.Nodes() {
		node := GetNode(nodeID)
		if node == nil {
			continue
		}

		node.Update(now)
	}
}

// Run runs world.
func (world *World) Run() {
	world.stopper = make(chan bool)

	ticker := time.NewTicker(time.Second / 30)

	go func() {
		for {
			select {
			case <-ticker.C:
				world.Update(time.Now())
			case <-world.stopper:
				return
			}
		}
	}()
}

// Stop stops world.
func (world *World) Stop() {
	world.stopper <- true
}

func (world World) MessageGraph() message.Graph {
	messageGraph := message.Graph{}
	for _, node := range world.graph.Nodes() {
		messageGraph.Nodes = append(
			messageGraph.Nodes,
			message.Node{
				node.ID(),
				message.Vector{node.Position().X, node.Position().Y}})
	}
	for _, edge := range world.graph.Edges() {
		messageGraph.Edges = append(messageGraph.Edges, message.Edge{edge.ID, edge.From, edge.To})
	}

	return messageGraph
}

func (world World) EntityCreates() (entityCreates []message.EntityCreate) {
	for nodeID := range world.graph.Nodes() {
		node := GetNode(nodeID)
		for _, entity := range node.Entitys() {
			entityCreates = append(
				entityCreates,
				message.EntityCreate{
					node.ID(),
					message.Entity{
						entity.ID(),
						entity.UserID(),
						"Entity"}})
		}
	}

	return entityCreates
}

func (world World) FindPath(fromNodeID, toNodeID int64) graph.Path {
	path, err := world.graph.Path(fromNodeID, toNodeID)
	if err != nil {
		return graph.Path{}
	}

	return path
}

func (world *World) createGraph() {
	offset := 200.0

	g := graph.NewGraph()
	g.AddNode(NewNode(1, world, vector.Vector{-3 * offset, -3 * offset}))
	g.AddNode(NewNode(2, world, vector.Vector{-1 * offset, -3 * offset}))
	g.AddNode(NewNode(3, world, vector.Vector{+1 * offset, -3 * offset}))
	g.AddNode(NewNode(4, world, vector.Vector{+3 * offset, -3 * offset}))
	g.AddNode(NewNode(5, world, vector.Vector{-4 * offset, -2 * offset}))
	g.AddNode(NewNode(6, world, vector.Vector{-2 * offset, -2 * offset}))
	g.AddNode(NewNode(7, world, vector.Vector{+0 * offset, -2 * offset}))
	g.AddNode(NewNode(8, world, vector.Vector{+2 * offset, -2 * offset}))
	g.AddNode(NewNode(9, world, vector.Vector{+4 * offset, -2 * offset}))
	g.AddNode(NewNode(10, world, vector.Vector{-5 * offset, -1 * offset}))
	g.AddNode(NewNode(11, world, vector.Vector{-3 * offset, -1 * offset}))
	g.AddNode(NewNode(12, world, vector.Vector{-1 * offset, -1 * offset}))
	g.AddNode(NewNode(13, world, vector.Vector{+1 * offset, -1 * offset}))
	g.AddNode(NewNode(14, world, vector.Vector{+3 * offset, -1 * offset}))
	g.AddNode(NewNode(15, world, vector.Vector{+5 * offset, -1 * offset}))
	g.AddNode(NewNode(16, world, vector.Vector{-6 * offset, +0 * offset}))
	g.AddNode(NewNode(17, world, vector.Vector{-4 * offset, +0 * offset}))
	g.AddNode(NewNode(18, world, vector.Vector{-2 * offset, +0 * offset}))
	g.AddNode(NewNode(19, world, vector.Vector{+0 * offset, +0 * offset}))
	g.AddNode(NewNode(20, world, vector.Vector{+2 * offset, +0 * offset}))
	g.AddNode(NewNode(21, world, vector.Vector{+4 * offset, +0 * offset}))
	g.AddNode(NewNode(22, world, vector.Vector{+6 * offset, +0 * offset}))
	g.AddNode(NewNode(23, world, vector.Vector{-5 * offset, +1 * offset}))
	g.AddNode(NewNode(24, world, vector.Vector{-3 * offset, +1 * offset}))
	g.AddNode(NewNode(25, world, vector.Vector{-1 * offset, +1 * offset}))
	g.AddNode(NewNode(26, world, vector.Vector{+1 * offset, +1 * offset}))
	g.AddNode(NewNode(27, world, vector.Vector{+3 * offset, +1 * offset}))
	g.AddNode(NewNode(28, world, vector.Vector{+5 * offset, +1 * offset}))
	g.AddNode(NewNode(29, world, vector.Vector{-4 * offset, +2 * offset}))
	g.AddNode(NewNode(30, world, vector.Vector{-2 * offset, +2 * offset}))
	g.AddNode(NewNode(31, world, vector.Vector{+0 * offset, +2 * offset}))
	g.AddNode(NewNode(32, world, vector.Vector{+2 * offset, +2 * offset}))
	g.AddNode(NewNode(33, world, vector.Vector{+4 * offset, +2 * offset}))
	g.AddNode(NewNode(34, world, vector.Vector{-3 * offset, +3 * offset}))
	g.AddNode(NewNode(35, world, vector.Vector{-1 * offset, +3 * offset}))
	g.AddNode(NewNode(36, world, vector.Vector{+1 * offset, +3 * offset}))
	g.AddNode(NewNode(37, world, vector.Vector{+3 * offset, +3 * offset}))

	g.AddEdge(1, 2)
	g.AddEdge(1, 5)
	g.AddEdge(1, 6)

	g.AddEdge(2, 1)
	g.AddEdge(2, 3)
	g.AddEdge(2, 6)
	g.AddEdge(2, 7)

	g.AddEdge(3, 2)
	g.AddEdge(3, 4)
	g.AddEdge(3, 7)
	g.AddEdge(3, 8)

	g.AddEdge(4, 3)
	g.AddEdge(4, 8)
	g.AddEdge(4, 9)

	g.AddEdge(5, 1)
	g.AddEdge(5, 6)
	g.AddEdge(5, 10)
	g.AddEdge(5, 11)

	g.AddEdge(6, 1)
	g.AddEdge(6, 2)
	g.AddEdge(6, 5)
	g.AddEdge(6, 7)
	g.AddEdge(6, 11)
	g.AddEdge(6, 12)

	g.AddEdge(7, 2)
	g.AddEdge(7, 3)
	g.AddEdge(7, 6)
	g.AddEdge(7, 8)
	g.AddEdge(7, 12)
	g.AddEdge(7, 13)

	g.AddEdge(8, 3)
	g.AddEdge(8, 4)
	g.AddEdge(8, 7)
	g.AddEdge(8, 9)
	g.AddEdge(8, 13)
	g.AddEdge(8, 14)

	g.AddEdge(9, 4)
	g.AddEdge(9, 8)
	g.AddEdge(9, 14)
	g.AddEdge(9, 15)

	g.AddEdge(10, 5)
	g.AddEdge(10, 11)
	g.AddEdge(10, 16)
	g.AddEdge(10, 17)

	g.AddEdge(11, 5)
	g.AddEdge(11, 6)
	g.AddEdge(11, 10)
	g.AddEdge(11, 12)
	g.AddEdge(11, 17)
	g.AddEdge(11, 18)

	g.AddEdge(12, 6)
	g.AddEdge(12, 7)
	g.AddEdge(12, 11)
	g.AddEdge(12, 13)
	g.AddEdge(12, 18)
	g.AddEdge(12, 19)

	g.AddEdge(13, 7)
	g.AddEdge(13, 8)
	g.AddEdge(13, 12)
	g.AddEdge(13, 14)
	g.AddEdge(13, 19)
	g.AddEdge(13, 20)

	g.AddEdge(14, 8)
	g.AddEdge(14, 9)
	g.AddEdge(14, 13)
	g.AddEdge(14, 15)
	g.AddEdge(14, 20)
	g.AddEdge(14, 21)

	g.AddEdge(15, 9)
	g.AddEdge(15, 14)
	g.AddEdge(15, 21)
	g.AddEdge(15, 22)

	g.AddEdge(16, 10)
	g.AddEdge(16, 17)
	g.AddEdge(16, 23)

	g.AddEdge(17, 10)
	g.AddEdge(17, 11)
	g.AddEdge(17, 16)
	g.AddEdge(17, 18)
	g.AddEdge(17, 23)
	g.AddEdge(17, 24)

	g.AddEdge(18, 11)
	g.AddEdge(18, 12)
	g.AddEdge(18, 17)
	g.AddEdge(18, 19)
	g.AddEdge(18, 24)
	g.AddEdge(18, 25)

	g.AddEdge(19, 12)
	g.AddEdge(19, 13)
	g.AddEdge(19, 18)
	g.AddEdge(19, 20)
	g.AddEdge(19, 25)
	g.AddEdge(19, 26)

	g.AddEdge(20, 13)
	g.AddEdge(20, 14)
	g.AddEdge(20, 19)
	g.AddEdge(20, 21)
	g.AddEdge(20, 26)
	g.AddEdge(20, 27)

	g.AddEdge(21, 14)
	g.AddEdge(21, 15)
	g.AddEdge(21, 20)
	g.AddEdge(21, 22)
	g.AddEdge(21, 27)
	g.AddEdge(21, 28)

	g.AddEdge(22, 15)
	g.AddEdge(22, 21)
	g.AddEdge(22, 28)

	g.AddEdge(23, 16)
	g.AddEdge(23, 17)
	g.AddEdge(23, 24)
	g.AddEdge(23, 29)

	g.AddEdge(24, 17)
	g.AddEdge(24, 18)
	g.AddEdge(24, 23)
	g.AddEdge(24, 25)
	g.AddEdge(24, 29)
	g.AddEdge(24, 30)

	g.AddEdge(25, 18)
	g.AddEdge(25, 19)
	g.AddEdge(25, 24)
	g.AddEdge(25, 26)
	g.AddEdge(25, 30)
	g.AddEdge(25, 31)

	g.AddEdge(26, 19)
	g.AddEdge(26, 20)
	g.AddEdge(26, 25)
	g.AddEdge(26, 27)
	g.AddEdge(26, 31)
	g.AddEdge(26, 32)

	g.AddEdge(27, 20)
	g.AddEdge(27, 21)
	g.AddEdge(27, 26)
	g.AddEdge(27, 28)
	g.AddEdge(27, 32)
	g.AddEdge(27, 33)

	g.AddEdge(28, 21)
	g.AddEdge(28, 22)
	g.AddEdge(28, 27)
	g.AddEdge(28, 33)

	g.AddEdge(29, 23)
	g.AddEdge(29, 24)
	g.AddEdge(29, 30)
	g.AddEdge(29, 34)

	g.AddEdge(30, 24)
	g.AddEdge(30, 25)
	g.AddEdge(30, 29)
	g.AddEdge(30, 31)
	g.AddEdge(30, 34)
	g.AddEdge(30, 35)

	g.AddEdge(31, 25)
	g.AddEdge(31, 26)
	g.AddEdge(31, 30)
	g.AddEdge(31, 32)
	g.AddEdge(31, 35)
	g.AddEdge(31, 36)

	g.AddEdge(32, 26)
	g.AddEdge(32, 27)
	g.AddEdge(32, 31)
	g.AddEdge(32, 33)
	g.AddEdge(32, 36)
	g.AddEdge(32, 37)

	g.AddEdge(33, 27)
	g.AddEdge(33, 28)
	g.AddEdge(33, 32)
	g.AddEdge(33, 37)

	g.AddEdge(34, 29)
	g.AddEdge(34, 30)
	g.AddEdge(34, 35)

	g.AddEdge(35, 30)
	g.AddEdge(35, 31)
	g.AddEdge(35, 34)
	g.AddEdge(35, 36)

	g.AddEdge(36, 31)
	g.AddEdge(36, 32)
	g.AddEdge(36, 35)
	g.AddEdge(36, 37)

	g.AddEdge(37, 32)
	g.AddEdge(37, 33)
	g.AddEdge(37, 36)

	world.graph = g
}
