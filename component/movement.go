package component

import (
	"math"

	"gitlab.com/legionary/legionary/core/math/rotator"
	"gitlab.com/legionary/legionary/core/math/vector"
)

// MovementComponent is component that present move.
type MovementComponent struct {
	owner                   movementComponentOwner
	targetPosition          *vector.Vector
	maxSpeed                float64
	speed                   float64
	acceleration            float64
	deceleration            float64
	decelerationStartLength float64
	rotationSpeed           float64
}

type movementComponentOwner interface {
	SetPosition(vector.Vector)
	Position() vector.Vector
	SetRotation(rotator.Rotator)
	Rotation() rotator.Rotator
}

// Init initialize.
func (m *MovementComponent) Init(owner movementComponentOwner) {
	m.owner = owner
	m.maxSpeed = 100
	m.acceleration = 50
	m.deceleration = 100
	m.decelerationStartLength = 200
	m.rotationSpeed = 90.0 * math.Pi / 180.0
}

// Update update.
func (m *MovementComponent) Update(delta float64) {
	if m.targetPosition == nil {
		return
	}

	diff := m.targetPosition.Sub(m.owner.Position())
	diffSize := diff.Size()
	if m.decelerationStartLength < diffSize {
		m.accelerate(delta)
	} else {
		desiredSpeed := m.maxSpeed * -(math.Pow(diffSize/m.decelerationStartLength-1, 4) - 1)

		if m.speed < desiredSpeed {
			m.accelerate(delta)
		} else {
			m.decelerate(delta, desiredSpeed)
		}
	}

	direction := diff.Nomalize()

	rotation := m.owner.Rotation()
	if 0 < rotation.Dir().Cross(direction) {
		rotation.Add(m.rotationSpeed * delta)
	} else {
		rotation.Add(-m.rotationSpeed * delta)
	}
	m.owner.SetRotation(rotation)

	m.owner.SetPosition(m.owner.Position().Add(rotation.Dir().Mul(m.speed * delta)))
}

// MoveTo move entity to target location.
func (m *MovementComponent) MoveTo(targetPosition vector.Vector) {
	m.targetPosition = &targetPosition
}

func (m *MovementComponent) Stop() {
	m.targetPosition = nil
}

func (m *MovementComponent) accelerate(delta float64) {
	if m.speed < m.maxSpeed {
		m.speed += m.acceleration * delta

		if m.maxSpeed < m.speed {
			m.speed = m.maxSpeed
		}
	}
}

func (m *MovementComponent) decelerate(delta, desiredSpeed float64) {
	m.speed -= m.deceleration * delta

	if m.speed < desiredSpeed {
		m.speed = desiredSpeed
	}
}
