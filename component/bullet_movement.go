package component

import (
	"gitlab.com/legionary/legionary/core/math/rotator"
	"gitlab.com/legionary/legionary/core/math/vector"
)

type BulletMovementComponent struct {
	owner bulletMovementComponentOwner
	speed float64
}

type bulletMovementComponentOwner interface {
	SetPosition(vector.Vector)
	Position() vector.Vector
	Rotation() rotator.Rotator
}

func (comp *BulletMovementComponent) Init(owner movementComponentOwner) {
	comp.owner = owner
	comp.speed = 100
}

func (comp *BulletMovementComponent) Update(delta float64) {
	comp.owner.SetPosition(comp.owner.Position().Add(comp.owner.Rotation().Dir().Mul(comp.speed * delta)))
}
