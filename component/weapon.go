package component

import (
	"gitlab.com/legionary/legionary/core/math/rotator"
	"gitlab.com/legionary/legionary/core/math/vector"
)

type WeaponComponent struct {
	owner weaponComponentOwner
	world world
}

type weaponComponentOwner interface {
	TeamId() int64
	Position() vector.Vector
	Rotation() rotator.Rotator
}

type world interface {
	CreateBullet(position vector.Vector, rotation rotator.Rotator, teamId int64)
}

func (comp *WeaponComponent) Init(owner weaponComponentOwner, world world) {
	comp.owner = owner
	comp.world = world
}

func (comp WeaponComponent) Fire() {
	comp.world.CreateBullet(comp.owner.Position(), comp.owner.Rotation(), comp.owner.TeamId())
}
