package component

// HPComponent presents health point.
type HPComponent struct {
	owner hpComponentOwner
	maxHP int
	hp    int
}

type hpComponentOwner interface {
}

// Init initialises.
func (component *HPComponent) Init(owner hpComponentOwner) {
	component.owner = owner
	component.maxHP = 1000
	component.hp = component.maxHP
}

func (component HPComponent) MaxHP() int {
	return component.maxHP
}

// HP returns current health point.
func (component HPComponent) HP() int {
	return component.hp
}

// Damage does damage your hp.
func (component *HPComponent) Damage(damage int) {
	component.hp -= damage

	if component.hp < 0 {
		component.hp = 0
	}
}
