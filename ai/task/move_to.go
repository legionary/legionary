package task

import (
	"gitlab.com/legionary/legionary/core/ai"
	"gitlab.com/legionary/legionary/core/math/vector"
)

// MoveTo represts move to task.
type MoveTo struct {
	ai.Node

	owner moveToOwner
	bb    blackboard
}

type moveToOwner interface {
	Position() vector.Vector
	MoveTo(targetPosition vector.Vector)
}

type blackboard interface {
	TargetPosition() vector.Vector
}

// NewMoveTo creates MoveTo task.
func NewMoveTo(owner moveToOwner, bb blackboard) *MoveTo {
	moveTo := &MoveTo{}
	moveTo.owner = owner
	moveTo.bb = bb

	return moveTo
}

// Init inits sqeuence.
func (task *MoveTo) Init() {
}

// Update updates sequnce.
func (task *MoveTo) Update(delta float64) ai.State {
	task.owner.MoveTo(task.bb.TargetPosition())

	if task.owner.Position().Sub(task.bb.TargetPosition()).Size() < 100 {
		return ai.Success
	}

	return ai.Running
}
