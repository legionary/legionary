package task

import "gitlab.com/legionary/legionary/core/ai"

type Wait struct {
	ai.Node

	time        float64
	waitingTime float64
}

func NewWait(time float64) *Wait {
	wait := &Wait{}
	wait.time = time

	return wait
}

func (task *Wait) Init() {
	task.waitingTime = 0
}

func (task *Wait) Update(delta float64) ai.State {
	task.waitingTime += delta

	if task.waitingTime < task.time {
		return ai.Running
	}

	return ai.Success
}
