package task

import (
	"gitlab.com/legionary/legionary/core/ai"
)

type Fire struct {
	ai.Node

	owner fireOwner
}

type fireOwner interface {
	Fire()
}

func NewFire(owner fireOwner) *Fire {
	fire := &Fire{}
	fire.owner = owner

	return fire
}

func (task *Fire) Update(delta float64) ai.State {
	task.owner.Fire()

	return ai.Success
}
