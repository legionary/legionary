package task

import (
	"gitlab.com/legionary/legionary/core/ai"
	"gitlab.com/legionary/legionary/core/math/vector"
)

type MoveToPosition struct {
	ai.Node

	owner    moveToOwner
	position vector.Vector
}

type moveToPositionOwner interface {
	Position() vector.Vector
	MoveTo(targetPosition vector.Vector)
}

func NewMoveToPosition(owner moveToPositionOwner, position vector.Vector) *MoveToPosition {
	task := &MoveToPosition{}
	task.owner = owner
	task.position = position

	return task
}

func (task *MoveToPosition) Init() {
}

func (task *MoveToPosition) Update(delta float64) ai.State {
	task.owner.MoveTo(task.position)

	if task.owner.Position().Sub(task.position).Size() < 100 {
		return ai.Success
	}

	return ai.Running
}
