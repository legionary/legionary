package message

//go:generate msgp

type EntityCreateReq struct {
	NodeID int64 `msg:"node_id"`
}

func (entityCreateReq EntityCreateReq) MessageID() uint32 {
	return MessageEntityCreateReq
}
