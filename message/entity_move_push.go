package message

//go:generate msgp

type EntityMovePush struct {
	EntityID   int64 `msg:"entity_id"`
	FromNodeID int64 `msg:"from_node_id"`
	ToNodeID   int64 `msg:"to_node_id"`
}

func (entityMovePush EntityMovePush) MessageID() uint32 {
	return MessageEntityMovePush
}
