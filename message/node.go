package message

//go:generate msgp

type Node struct {
	ID       int64  `msg:"id"`
	Position Vector `msg:"position"`
}
