package message

//go:generate msgp

type EntityCreate struct {
	NodeID int64  `msg:"node_id"`
	Entity Entity `msg:"entity"`
}
