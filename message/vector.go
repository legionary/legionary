package message

//go:generate msgp

type Vector struct {
	X float64 `msg:"x"`
	Y float64 `msg:"y"`
}
