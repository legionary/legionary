package message

//go:generate msgp

type Edge struct {
	ID   int64 `msg:"id"`
	From int64 `msg:"from"`
	To   int64 `msg:"to"`
}
