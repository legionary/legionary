package message

//go:generate msgp

type WorldReq struct {
}

func (worldReq WorldReq) MessageID() uint32 {
	return MessageWorldReq
}
