package message

//go:generate msgp

type Entity struct {
	ID     int64  `msg:"id"`
	UserID int64  `msg:"user_id"`
	Name   string `msg:"name"`
}
