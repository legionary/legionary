package message

//go:generate msgp

type EntityCreatePush struct {
	EntityCreate []EntityCreate `msg:"entity_creates"`
}

func (entityCreatePush EntityCreatePush) MessageID() uint32 {
	return MessageEntityCreatePush
}
