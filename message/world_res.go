package message

//go:generate msgp

type WorldRes struct {
	Graph Graph `msg:"graph"`
}

func (worldRes WorldRes) MessageID() uint32 {
	return MessageWorldRes
}
