package message

const (
	MessageEntityCreateReq  = 1
	MessageEntityCreatePush = 2
	MessageEntityMovePush   = 3
	MessageWorldReq         = 4
	MessageWorldRes         = 5
)
