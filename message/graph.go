package message

//go:generate msgp

type Graph struct {
	Nodes []Node `msg:"nodes"`
	Edges []Edge `msg:"edges"`
}
