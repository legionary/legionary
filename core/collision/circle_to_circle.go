package collision

import "gitlab.com/legionary/legionary/core/math/vector"

// CircleToCircle returns true when two circles collide.
func CircleToCircle(lhs, rhs circle) bool {
	diff := lhs.Position().Sub(rhs.Position())

	sumRadius := lhs.Radius() + rhs.Radius()

	if diff.SizeSquare() <= sumRadius*sumRadius {
		return true
	}

	return false
}

type circle interface {
	Position() vector.Vector
	Radius() float64
}
