package collision

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/legionary/legionary/core/math/vector"
)

func TestCircleToCircle(t *testing.T) {
	tests := map[string]struct {
		lhs    Circle
		rhs    Circle
		result bool
	}{
		"1": {
			lhs:    Circle{vector.Vector{X: 0, Y: 0}, 100},
			rhs:    Circle{vector.Vector{X: 0, Y: 0}, 100},
			result: true,
		},
		"2": {
			lhs:    Circle{vector.Vector{X: -100, Y: 0}, 100},
			rhs:    Circle{vector.Vector{X: 100, Y: 0}, 100},
			result: true,
		},
		"3": {
			lhs:    Circle{vector.Vector{X: 0, Y: 0}, 41},
			rhs:    Circle{vector.Vector{X: 100, Y: 100}, 100},
			result: false,
		},
	}

	for testName, test := range tests {
		t.Logf("Running test case %s", testName)

		result := CircleToCircle(test.lhs, test.rhs)
		assert.Equal(t, test.result, result, "lhs: %v, rhs: %v", test.lhs, test.rhs)
	}
}
