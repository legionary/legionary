package collision

import (
	"gitlab.com/legionary/legionary/core/math/vector"
)

type Circle struct {
	position vector.Vector
	radius   float64
}

func NewCircle(position vector.Vector, radius float64) *Circle {
	circle := &Circle{
		position,
		radius,
	}

	return circle
}

func (c Circle) Position() vector.Vector {
	return c.position
}

func (c Circle) Radius() float64 {
	return c.radius
}
