package collision

import (
	"gitlab.com/legionary/legionary/core/math/vector"
)

type Bullet struct {
	position vector.Vector
}

func (b Bullet) Position() vector.Vector {
	return b.position
}
