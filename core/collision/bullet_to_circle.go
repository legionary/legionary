package collision

import "gitlab.com/legionary/legionary/core/math/vector"

// BulletToCircle returns true when bullet and circle collide.
func BulletToCircle(lhs bullet, rhs Circle) bool {
	diff := lhs.Position().Sub(rhs.Position())

	if diff.SizeSquare() <= rhs.Radius()*rhs.Radius() {
		return true
	}

	return false
}

type bullet interface {
	Position() vector.Vector
}
