package shape

import (
	"gitlab.com/legionary/legionary/core/math/vector"
)

// Circle represents geometry circle.
type Circle interface {
	Position() vector.Vector
	SetPosition(vector.Vector)
	Radius() float64
}
