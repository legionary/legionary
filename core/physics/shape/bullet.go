package shape

import (
	"gitlab.com/legionary/legionary/core/math/vector"
)

type Bullet interface {
	Position() vector.Vector
	SetPosition(vector.Vector)
}
