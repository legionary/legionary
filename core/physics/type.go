package physics

// Shape represents rigidbody shape.
type Shape int

// Enum type for shapes.
const (
	Bullet Shape = 0
	Circle Shape = 1
)
