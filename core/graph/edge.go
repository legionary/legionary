package graph

type Edge struct {
	ID   int64
	From int64
	To   int64
}
