package main

import (
	"github.com/gorilla/websocket"
	"gitlab.com/legionary/legionary/core/net"
	"gitlab.com/legionary/legionary/game"
)

func main() {
	world := game.NewWorld()
	world.Run()
	defer world.Stop()

	server := net.NewServer(
		func(conn *websocket.Conn) net.User {
			return game.NewUser(conn, world)
		})
	server.ListenAndServe()
}
